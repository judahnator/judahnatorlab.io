---
layout: post
title:  "Visual Quota Representations - Part 2"
date:   2017-03-29 20:05:00 -0500
categories: first
---
## Visual Quota Representation Part 2 - The Model
In part 2 I will cover the things needed to ensure that pushing the migration live does not interfere with normal site operations.
The biggest part of this is ensuring the model knows how to access and serve the new information.

### The File Model
Now when deploying database changes to production it is vital to ensure there are absolutely no breaking changes. Luckily the logic behind ensuring nothing breaks after deployment fits nicely in an accessor and mutator. We just need to cache whether or not the new "size" column exists.

All of the following go into the File model:

**The Constructor**
{% highlight php startinline=true %}
if (!\Cache::has('file_migration_ran')) {
    \Cache::put(
        'file_migration_ran',
        Schema::hasColumn($this->getTable(), 'size'),
        1
    );
}
{$ endhighlight %}

**The Accessor**
{% highlight php startinline=true %}
if (\Cache::get('file_migration_ran',false)) {
    $Size = $value;
}else {
    $Size = \File::size(storage_path('app/files/'.$this->internalName));
}
{% endhighlight %}

**The Mutator**
{% highlight php startinline=true %}
if (\Cache::get('file_migration_ran',false)) {
    $this->attributes['size'] = $value['raw'];
}
{% endhighlight %}

### There is still one problem
Besides creating the model, manually calculating the filesize yourself, setting it yourself, then re-saving the model, there is no way to set the files size.

What we will need to do before deploying this change is create an event that fires after a file has been uploaded, which calculates its size and updates the model.

That will come in another post though, stay tuned!
