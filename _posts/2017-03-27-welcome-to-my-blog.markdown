---
layout: post
title:  "Welcome to my blog!"
date:   2017-03-27 11:32:14 -0500
categories: first
---
Welcome to my blog.

I decided to try something new and give GitLab a shot. So far I am really liking it.

So, why is this a thing? Well, I needed a place to talk to myself about all the nerdy projects I am working on. Sometimes I need to get a rant out, or maybe I just want to walk through why I am doing the things I do with a friend.

In any case, look forward to exciting content in the near future. Don't get your hopes up though...


~Judah
