---
layout: post
title:  "Visual Quota Representations"
date:   2017-03-30 20:30:00 -0500
categories: development quota holdthis
---
## Visual Quota Representation - The introduction
I found myself wanting a more visual representation of how users were measuring up against their disk quotas. Not everyone likes doing some mental math to figure how many of whatever storage unit they have left.

Sometimes its easier to just look and see "Oh, I'm at about 40%."

Going about doing this shouldn't be too terribly difficult. We could even use the fancy progress bars provided by Bootstrap.

### Some initial assumptions
* The number of files a user may upload is unlimited
* The size of the files a user may upload is (in theory) unlimited
* Disk usage calculation happens in timed byte-hours converted to GB per month
  * Indexing will happen in 1-hour intervals
  * There are 720 hours in a month
  * This means we need to take the hourly sum of allocated bytes, divide by 720 to get bytes/month, and divide by 1024^3 to get gigabytes/month.


### Problems presented

#### Calculating deleted file sizes
The biggest one will be that files deleted wont be able to be counted against the total monthly usage. Even though once deleted they are no longer taking up disk space, the fact remains that it took up space at some point.

This can be most easily counteracted by adding a "size" column to the Files table. To do that we will need to create a new migration file. Normally I would simply edit the existing file, but because the system is now in production mode it becomes a little more tedious.

#### How FileSize is retrieved
Because the method of data retrieval is being changed, combined with the fact that this data is being used in a production environment, means we have some work to do. Lucky for us the format of the data does not need to change, only how the data is being stored.

Previously the system had simply been querying the file-system for the desired files size. Understandably this will not scale well. Not only that but because the uploaded files will not be changing in size we are just taxing the drive more than necessary.

The new system will be much simpler. Instead of the sizes being calculated on the fly when requested, they will be counted at the time of creation and pulled from the database instead of the filesystem.

So, where does the problem come in?
The problem is that the site should not go offline, even for a moment. This means we will need to code some redundancy into the File model to double check to see if the new column exists before attempting to read or write to it.

#### How FileSize is stored
Retrieving the data is only half the battle. The other half is setting it.

Normally I would add the item as a `$fillable` attribute, but in the best interest of keeping code clean (and people honest) I don't think I will trust that input. Not only that, but I want to keep data type consistency. When "getting" or "setting" the size attribute the data type expected is a collection. Although unused, this collection does contain a "human" element that wont always be known. 

The details on how I will go about making this work will come in a future blog post.

#### Garbage collection
After all of the above has been completed we will have a soft-delete enabled model which holds the size of both current and deleted files. Now I REALLY don't want to be holding onto any data longer than I need to, even if its just a few rows in a database.

This means that some method of "garbage collection" will be necessary to clean up database records whose files have expired more than a month prior.

### Summary
There is a good deal of work to do on this. To be perfectly honest I tried to fit it all into a single post, but with code snippets it turned into a bit of a bear. Instead, I will do a multi-part series.

Stay tuned for the incoming updates, and be sure to check back on this page for links and edits.