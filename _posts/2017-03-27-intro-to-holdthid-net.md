---
layout: post
title: "Introduction to HoldThis.net"
date: 2017-03-27 15:06:00 -0500
categories: holdthis introduction development
---
## HoldThis.net
This will be the beginning of a blog series of the site https://holdthis.net.

### Why start this project?
Have you ever had a file you wanted to get from point A to B, but Dropbox or Google drive were too much hassle? Or maybe you did not want to deal with complicated permission schemes after uploading? Wouldn't it be awesome if there were a pastebin or pasteboard for generic files?

Well that's what I have made. You have some bare minimums, notes and expiration dates and visibility. Keeping speed and efficiency in mind all of those options are either optional or have default options. Its not rocket science, its file storage.

### What is the use case?
Why and how you use this site is up to the individual user. In my case I needed a place to easily upload and store files temporary, with an open and publicly documented API.
Some examples of how it could be used are maybe quickly sharing a file for a school project with your peers, or maybe holding an off-site server backup while you test something.

### Will it be free?
There will be a free tier. Storage is cheap and I am not expecting to make money on this project, so the free tier should apply to the vast majority of users. I would like, however, a paid option for individuals who have higher storage or bandwidth demands.
The most likely option for a pricing model is by timed byte-hours. Lets say 1GB/mo. If you divide 30 (days/month) by the number of days you need a file stored for, you get the maximum file size you can store. On the flip side, if you take 1GB and divide it by the size of your file you get the maximum duration it can be stored for.

So, for example, you could store a 1GB file for a month or a 30GB file for a day for free. Anything after that will likely belong in a paid tier.

To start it will be 100% free for all users, but stay tuned for updates.

### Are my files secure?
Is this even a question? You are trusting a third party with your data.
However, just so some things are in the open...
* Yes, I keep server logs. This project is in development and I need to troubleshoot.
* Your files are uploaded via HTTPS, so eavesdropping is not an issue.
* When your files expire they are actually deleted, but I have no control over copies people downloaded.

Now if you encrypt your files before uploading and set the access level to "private," you should be fine. I am not responsible for you doing anything stupid though.

### Summary
To sum it all up I hope you find this project as useful as I do. If there are any questions or concerns my contact information is scattered around this site.


Stay tuned for future posts and developments!
