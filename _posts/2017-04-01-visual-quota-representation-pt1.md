---
layout: post
title:  "Visual Quota Representations - Part 1"
date:   2017-04-01 11:00:00 -0500
categories: development quota holdthis migration
---
## Visual Quota Representation Part 1 - The Migration
Part 1 will cover the migration file needed to store the new data. In this post I will talk about all the details going into the migration file, as well as why I am doing the things I do.

<!-- more -->

### The migration file
Laravel makes life easy when generating commonly used files. To make the new migration I simply run this command:

{% highlight bash %}
php artisan make:migration --table=files add_size_to_files_table
{% endhighlight %}

That will generate a blueprint for us to use. It will take care of the most of the work for us. I had to strip the comments out because they were causing issues with my editor, but the end result will look something similar to this:

{% highlight php %}
<?php

use Illuminate\\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSizeToFilesTable extends Migration
{

    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->unsignedBigInteger('size')
                ->default(0)
                ->after('visibility');
            $table->softDeletes()->after('updated_at');
        });
    }


    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('size');
            $table->dropSoftDeletes();
        });
    }
}
{% endhighlight %}

One thing of note is that the normal Integer data type was not enough. With an unsigned integer, we could report a maximum of a 4GB file. We will need to use a big integer. Now, since a filesize cannot be negative, why not make it an unsigned big integer?

I am also adding the softdelete column. We will also need to add the `softdelete` trait to the model, but because the softDeletes `deleted_at` column defaults to null we wont need to take any action immediately.

Because we ensured we had a default value for the field, we can push this to production without worrying about anything breaking. Besides, we will be running this migration manually once the model is ready.

In the next post I will talk about what I am doing to the File model.
