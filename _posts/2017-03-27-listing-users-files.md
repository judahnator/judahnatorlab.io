---
layout: post
title: "Listing a Users Files"
date: 2017-03-27 16:31:00 -0500
categories: holdthis development
---
This is the second entry in the [HoldThis.net](https://holdthis.net) dev blog.

I have not yet settled upon a good format, so hang tight while we figure this out.

### The Problem To Solve
At the moment users cannot view all the files they have uploaded. This means that unless they happen to know what the ID of their uploaded file is, they cannot view their files. This could become an issue if someone loses the link to a file, and has to re-upload the item. This takes up more disk space and bandwidth, all while being an issue easily avoided.

### My Steps To Solve

First things first is I needed to find a good home for the data to be displayed. There is already somewhat of a dashboard the user sees when they first log in. This would be a good place.

A normal HTML table should do nicely. I will include almost all of the data I have in it, wanting to be as helpful as possible to the user and all. Of the data to include, I think the following will be a good list:
* File Name
* Notes
* Visibility
* Downloads
* Expires At

The first thing I noticed when generating that table was some users may leave a lot of notes. This shouldn't be much of an issue, we can do something like this to truncate the notes if they get too long:
{% highlight php %}
echo strlen($file->notes) > 20 ? substr($file->notes,0,20).'...' : $file->notes;
{% endhighlight %}

Another thing to consider is the ordering of the items. My first thought was to order the items by the "expires at" column so the user could see at a glance what everythings status is. That could get rather annoying, especially since the entire point of the site is to have the files removed after a certain amount of time. Instead I think the files should be sorted alphabetically.

{% highlight php %}
foreach (\Auth::user()->files->sortBy('clientName') as $file) {
  // Table Rows
}
{% endhighlight %}

Maybe in the future I will color-code the expires-at column to indicate how long until expiration, but for now its fine.

There should also be something that displays should the user not have any files associated with their account. An empty table is an ugly table.
We don't need to go overboard, so a simple bootstrap alert will do nicely.
{% highlight html %}
<div class="alert alert-info">
  There are no files currently associated with your account
</div>
{% endhighlight %}

Now that the table is just about done, we need to make sure the user can easily navigate back to this page should they need to. I added an item to the dropdown menu item with a "home" link, that should do the trick.
{% highlight html %}
{% raw %}
<ul class="dropdown-menu">
    <li><a href="{{ url('home') }}">Home</a></li>
    <li>
        <a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            Logout
        </a>
    </li>
</ul>
{% endraw %}
{% endhighlight %}

That just about wraps everything I did today up. If there are any questions or comments feel free to bug me on [Twitter](https://twitter.com/judahwright).
